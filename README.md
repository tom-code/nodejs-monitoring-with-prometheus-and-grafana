# Node.js Monitoring with Prometheus and Grafana

This repository contains two Node.js server demos. An example how to use custom Prometheus metrics can be found in [/custom-nodesjs-metrics](./custom-nodejs-metrics). An example how to setup a Node.js server for the [RED](https://grafana.com/blog/2018/08/02/the-red-method-how-to-instrument-your-services/) monitoring methodology can be found in [/example-nodejs-app](./example-nodejs-app). Both examples expose metrics via their `/metrics` endpoint. The metrics are periodically scraped by [Prometheus](https://prometheus.io) and visualized through a [Grafana](https://grafana.com/oss/grafana) monitoring dashboard.

## Prerequisites
Make sure that you have Docker and Docker Compose installed:
- [Docker Engine](https://docs.docker.com/engine)
- [Docker Compose](https://docs.docker.com/compose)

## Getting started
Clone the repository:
```bash
git clone https://gitlab.com/tom-code/nodejs-monitoring-with-prometheus-and-grafana.git
```

Navigate into the project directory:
```bash
cd nodejs-monitoring-with-prometheus-and-grafana
```

 Start the Docker containers:
```bash
docker-compose up -d
```

## Test containers
- Prometheus should be accessible via [http://localhost:9090](http://localhost:9090)
- Grafana should be accessible via [http://localhost:3000](http://localhost:3000)
- Custom Node.js metrics should be accessible via [http://localhost:8081/metrics](http://localhost:8081/metrics)
- Example Node.js server metrics for RED monitoring should be accessible via [http://localhost:8080/metrics](http://localhost:8080/metrics)

## Open monitoring dashboards
Open in your web browser the monitoring dashboards:
- Custom Node.js metrics dashboard can be found on [http://localhost:3000/d/32mcediMz/custom-metrics-dashboard](http://localhost:3000/d/32mcediMz/custom-metrics-dashboard)
- RED monitoring dashboard for the Node.js example server can be found on[http://localhost:3000/d/1DYaynomMk/example-service-dashboard](http://localhost:3000/d/1DYaynomMk/example-service-dashboard)
