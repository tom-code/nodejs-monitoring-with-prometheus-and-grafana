const http = require('http')
const url = require('url');
const client = require('prom-client')

const register = new client.Registry()

register.setDefaultLabels({
  app: 'custom-nodejs-metrics'
})

client.collectDefaultMetrics({ register })

const counter = new client.Counter({
  name: 'example_counter',
  help: 'This is an example counter'
})

const gauge = new client.Gauge({
  name: 'example_gauge',
  help: 'This is an example gauge'
})

const histogram = new client.Histogram({
  name: 'example_histogram',
  help: 'This is an example histogram',
  buckets: client.linearBuckets(1, 2, 5), // [ 1, 3, 5, 7, 9 ]
})

const summary = new client.Summary({
  name: 'example_summary',
  help: 'This an example summary',
})

register.registerMetric(counter)
register.registerMetric(gauge)
register.registerMetric(histogram)
register.registerMetric(summary)

const randomNumber = (min, max) => Math.random() * (max - min) + min

setInterval(() => {
  const num = randomNumber(0, 10)
  counter.inc()
  gauge.set(num)
  histogram.observe(num)
  summary.observe(num)
}, 1000)

const metricsServer = http.createServer((req, res) => {
  const route = url.parse(req.url).pathname;

  if (route === '/metrics') {
    res.setHeader('Content-Type', register.contentType)
    res.end(register.metrics())
    return
  }

  res.writeHead(404).end()
})

metricsServer.listen(8081, () => {
  console.log('Metrics are exposed on http://localhost:8081/metrics')
})
